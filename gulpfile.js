/**
 * Dependencias
 */
var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	minifycss = require('gulp-minify-css'),
	notify = require('gulp-notify'),
	browserSync = require('browser-sync').create(),
	reload = browserSync.reload;

/**
 * Configuración de tareas
 */
gulp.task('scripts', function(){
	gulp.src([
			'assets/js/jquery.min.js',
			'assets/js/popper.min.js',
			'assets/js/bootstrap-material-design.min.js',
			'assets/js/material-kit.js'
		])
	.pipe(concat('app.min.js'))
	.pipe(uglify({
		compress: {
			drop_console: true
		}
	}))
	// .pipe(uglify())
	.pipe(gulp.dest('assets/js/min/'))
	.pipe(browserSync.stream())
	.pipe(notify('Ha finalizado scripts'));
});

/**
 * estilos
 */
gulp.task('styles', function() {
	gulp.src([
			'assets/css/bootstrap.min.css',
			'assets/css/material-kit.css',
			'assets/css/responsive.css'
		])
	.pipe(concat('app.min.css'))
	.pipe(minifycss())
	.pipe(gulp.dest('assets/all/'))
	.pipe(browserSync.stream())
	.pipe(notify('Ha finalizado styles'));
});

/**
 * serve 
 */
gulp.task('serve', ['scripts', 'styles'], function() {
    browserSync.init({
        proxy: "http://tapiolalanding.test"
    });

    gulp.watch('assets/js/*.js', ['scripts']);
	gulp.watch('assets/css/*.css', ['styles']);
	gulp.watch("*.html").on('change', reload);
});

/**
 * observar cambios en js y css
 */
gulp.task('watch', function() {
	gulp.watch('assets/js/*.js', ['scripts']);
	gulp.watch('assets/css/*.css', ['styles']);
});

/**
 * ejecutar tarea default
 */
// gulp.task('default', ['scripts', 'styles', 'watch']);
gulp.task('default', ['serve']);